<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ListView;

$this->title = 'new world';
?>
<div class="site-index">
    <div class="body-content">
        
       <!--Carrusel de inicio-->
    <div id="carrusel" class="carousel slide " data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <?= Html::img('@web/img/uploads/House.jpg',['class'=>'d-block w-100']) ?>

          </div>
          <div class="carousel-item">
            <?= Html::img('@web/img/uploads/LunarTiger.jpg',['class'=>'d-block w-100']) ?>
          </div>
          <div class="carousel-item">
            <?= Html::img('@web/img/uploads/Noticias.jpg',['class'=>'d-block w-100']) ?>
          </div>
        </div>
    </div>

       <!--Fin del carrusel-->
 
       
       <!--tarjetas de noticias-->
       <div class="row fondo-ng">
        <h2 class="text-center border-bottom p-2 "> Ultimas Noticias </h2>
       </div>
       <div class="row row-cols-1 row-cols-md-3 fondo-ng pt-3 ">
        
        
        <?= ListView::widget([
                'dataProvider'=>$dataProvider,
                'layout'=>"{items}",
                'itemView'=> function($model){
            
            $noticia =  $model['noticia'];
           
            $noticia =  substr($noticia, 0, (strpos($noticia, "</p>")-4));
            $noticia = strrchr($noticia,'">');
            $noticia = substr($noticia,2,150).'...';
           
            
        ?>
        <div class="col">
          <div class="card">
            <?= Html::img('@web/img/uploads/'.$model['banner_img'],['class'=>'card-img', 'height'=> '70%']) ?> 
             
            <div class="card-body ">
              <h5 class="card-title"> <?= $model['title'] ?> </h5>
              
              <div class="card-text"> <?= $noticia ?></div>
              
            </div>
              <div class="card-footer">
              <p><?= Html::a('Seguir leyendo la noticia...',['noticias/noticia','id'=>$model->id],['class'=>'btn btn-block']) ?></p>
              </div>
          </div>
        </div>
           
        <?php }]); ?>
      
       
       </div>
        
      
       <!--Fin de las noticias-->
       <div class="row fondo-ng pt-3 pb-3 ">
          <h2 class="text-center"> Mapa del Nuevo Mundo.</h2>
            <?= Html::img('@web/img/mapas/mapa-general.png', ['width'=>'70%','class'=>'mx-auto d-block']) ?> 
       </div>
       
    </div>
</div>
