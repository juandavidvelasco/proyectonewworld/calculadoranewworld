<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Zonas */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Zonas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="zonas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-outline-danger',
            'data' => [
                'confirm' => 'Seguro que desea elminar?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Crear Nuevo Registro', ['create'], ['class' => 'btn btn-danger float-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            [                      
                'label' => 'Mapa',
                'value' => function ($data) {
                    return '@web/img/mapas/'.$data->mapa;
                }, 
                'format' => ['image',['width'=>'400px']]
            ],
        ],
    ]) ?>

</div>
