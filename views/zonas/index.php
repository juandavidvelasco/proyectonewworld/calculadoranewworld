<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zonas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zonas-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Zonas', ['create'], ['class' => 'btn btn-danger']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
           
            [
              'label'=> 'Nombre de la zona',
              'attribute'=>'nombre',
              'headerOptions'=>['class'=>'text-center'],
              'contentOptions'=>['class'=>'text-center center mt-3']
            ],
            [                      
                'label' => 'Mapa',
                'value' => function ($data) {
                    return '@web/img/mapas/'.$data->mapa;
                }, 
                'format' => ['image',['width'=>'250px']],
                'headerOptions'=>['class'=>'text-center'],
                'contentOptions'=>['class'=>'text-center center mt-3']
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
