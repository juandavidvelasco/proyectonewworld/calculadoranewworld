<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProcesanComponentes */

$this->title = 'Update Procesan Componentes: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Procesan Componentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="procesan-componentes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profesiones' => $profesiones, 
        'componentes' => $componentes
    ]) ?>

</div>
