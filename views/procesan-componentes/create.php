<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProcesanComponentes */

$this->title = 'Create Procesan Componentes';
$this->params['breadcrumbs'][] = ['label' => 'Procesan Componentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procesan-componentes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profesiones' => $profesiones, 
        'componentes' => $componentes
    ]) ?>

</div>
