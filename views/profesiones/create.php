<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profesiones */

$this->title = 'Crear Profesiones';
$this->params['breadcrumbs'][] = ['label' => 'Profesiones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesiones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
