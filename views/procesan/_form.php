<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$comp = ArrayHelper::map($componentes,'id','nombre');
$ingre = ArrayHelper::map($ingrediente,'id','nombre');
$prof = ArrayHelper::map($profesiones,'id','nombres');
?>

<div class="procesan-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?=    $form->field($model, 'id_componente')->widget(Select2::classname(), [
                'data' => $comp,
                'options' => ['placeholder' => 'Seleccione un componente'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>
    <?=    $form->field($model, 'id_ingrediente')->widget(Select2::classname(), [
                'data' => $ingre,
                'options' => ['placeholder' => 'Seleccione un ingrediente'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?=    $form->field($model, 'id_profesion')->widget(Select2::classname(), [
                'data' => $prof,
                'options' => ['placeholder' => 'Seleccione una profesión'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?= $form->field($model, 'cantidad_ingrediente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
