<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Procesan */

$this->title = 'Modificar Procesan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Procesan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="procesan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profesiones' => $profesiones, 
        'ingrediente' => $ingrediente,  
        'componentes' => $componentes
    ]) ?>

</div>
