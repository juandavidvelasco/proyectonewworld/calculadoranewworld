<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Procesan */

$this->title = 'Crear Procesan';
$this->params['breadcrumbs'][] = ['label' => 'Procesan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="procesan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profesiones' => $profesiones, 
        'ingrediente' => $ingrediente,  
        'componentes' => $componentes
    ]) ?>

</div>
