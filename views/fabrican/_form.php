<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;


$util = ArrayHelper::map($utilizable,'id','nombre');
$comp = ArrayHelper::map($componentes,'id','nombre');
$prof = ArrayHelper::map($profesiones,'id','nombres');
?>

<div class="fabrican-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=    $form->field($model, 'id_utilizable')->widget(Select2::classname(), [
                'data' => $util,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?=    $form->field($model, 'id_componente')->widget(Select2::classname(), [
                'data' => $comp,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

   <?=    $form->field($model, 'id_profesion')->widget(Select2::classname(), [
                'data' => $prof,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?= $form->field($model, 'cantidad_componente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
