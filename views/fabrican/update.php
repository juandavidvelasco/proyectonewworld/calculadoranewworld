<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fabrican */

$this->title = 'Modificar Fabrican: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fabrican', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="fabrican-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'utilizable' => $utilizable,
        'componentes' => $componentes,
        'profesiones' => $profesiones
    ]) ?>

</div>
