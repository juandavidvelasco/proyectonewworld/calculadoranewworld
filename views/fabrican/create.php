<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fabrican */

$this->title = 'Crear Fabrican';
$this->params['breadcrumbs'][] = ['label' => 'Fabrican', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabrican-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'utilizable' => $utilizable,
        'componentes' => $componentes,
        'profesiones' => $profesiones
    ]) ?>

</div>
