<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fabrican';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabrican-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Registro', ['create'], ['class' => 'btn btn-danger']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            
            [
               'attribute'=> 'Utilizable',
                'value'=>'utilizable.nombre'
            ],
            [
               'attribute'=> 'Componente',
                'value'=>'componente.nombre'
            ],
            [
               'attribute'=> 'Profesion',
                'value'=>'profesion.nombres'
            ],
           
            'cantidad_componente',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
