<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fabrican */

$this->title = $model->utilizable->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Fabrican', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="fabrican-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-outline-danger',
            'data' => [
                'confirm' => 'Seguro que desea elminar?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Crear Nuevo Registro', ['create'], ['class' => 'btn btn-danger float-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'utilizable.nombre',
            'componente.nombre',
            'profesion.nombres',
            'cantidad_componente',
        ],
    ]) ?>

</div>
