<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

use kartik\widgets\FileInput;

$cate = ArrayHelper::map($categorias,'id','tipo');
$prof = ArrayHelper::map($profesiones,'id','nombres');
?>

<div class="ingredientes-form">

    <?php $form = ActiveForm::begin(); ?>

  
    <?=    $form->field($model, 'id_profesion')->widget(Select2::classname(), [
                'data' => $prof,
                'options' => ['placeholder' => 'Seleccione la profesión'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>
    
    <?=    $form->field($model, 'id_categoria')->widget(Select2::classname(), [
                'data' => $cate,
                'options' => ['placeholder' => 'Seleccione la categoria'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>
    

    <?= $form->field($model, 'experiencia')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'rareza')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tier')->textInput() ?>
    
    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
        'options'=>['accept'=>'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>
    
    <?php ActiveForm::end(); ?>

    
</div>
