<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Utilizables';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilizables-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Utilizables', ['create'], ['class' => 'btn btn-danger']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre',
            'exp_otorgada',
            'tier',
            'estadisticas',
            
            [                      
                'label' => 'Icono',
                'value' => function ($data) {
                    return '@web/img/thumbs/thumb_'.$data->icono; 
                } ,
                'format' => ['image',['width'=>'100px']]
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
