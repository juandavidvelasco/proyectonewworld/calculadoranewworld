<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Utilizables */

$this->title = 'Crear Utilizables';
$this->params['breadcrumbs'][] = ['label' => 'Utilizables', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="utilizables-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
