<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\FabricanIngrediente */
/* @var $form yii\widgets\ActiveForm */

$util = ArrayHelper::map($utilizable,'id','nombre');
$ing = ArrayHelper::map($ingredientes,'id','nombre');
$prof = ArrayHelper::map($profesiones,'id','nombres');
?>

<div class="fabrican-ingrediente-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=    $form->field($model, 'id_utilizable')->widget(Select2::classname(), [
                'data' => $util,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?=    $form->field($model, 'id_ingrediente')->widget(Select2::classname(), [
                'data' => $ing,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

   <?=    $form->field($model, 'id_profesion')->widget(Select2::classname(), [
                'data' => $prof,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?= $form->field($model, 'cantidad_componente')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
