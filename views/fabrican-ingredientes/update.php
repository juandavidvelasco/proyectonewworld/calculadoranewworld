<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FabricanIngrediente */

$this->title = 'Update Fabrican Ingrediente: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Fabrican Ingredientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fabrican-ingrediente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'utilizable'=>$utilizable,
        'profesiones'=>$profesiones,
        'ingredientes'=>$ingredientes
    ]) ?>

</div>
