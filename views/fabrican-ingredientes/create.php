<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FabricanIngrediente */

$this->title = 'Create Fabrican Ingrediente';
$this->params['breadcrumbs'][] = ['label' => 'Fabrican Ingredientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fabrican-ingrediente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'utilizable'=>$utilizable,
        'profesiones'=>$profesiones,
        'ingredientes'=>$ingredientes
    ]) ?>

</div>
