<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Componentes */

$this->title = 'Crear Componentes';
$this->params['breadcrumbs'][] = ['label' => 'Componentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="componentes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
