<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ListView;

?> 
<div class="principa-noticia">
    <div class="body-content">
        <div class="card bg-dark text-white mb-3 ">
            <?= Html::img('@web/img/banner_1.jpg',['height'=>'60%']) ?>  
           <div class="card-img-overlay">
           <h1 class="text-right">Todas las noticias</h1>
           </div>
        </div>
        <div class="row row-cols-1 row-cols-md-3 ">
        <?= ListView::widget([
                'dataProvider'=>$dataProvider,
                'layout'=>"{items}",
                'itemView'=> function($model){
            
            $noticia =  $model['noticia'];
           
            $noticia =  substr($noticia, 0, (strpos($noticia, "</p>")-4));
            $noticia = strrchr($noticia,'">');
            $noticia = substr($noticia,2,150).'...';
           
            
        ?>
        <div class="col">
          <div class="card">
            <?= Html::img('@web/img/uploads/'.$model['banner_img'],['class'=>'card-img', 'height'=> '70%']) ?> 
            <div class="card-body">
                <h5 class="card-title"> <?= $model['title'] ?> </h5>
              
                <div class="card-text"> <?= $noticia ?></div>

            </div>
            <div class="card-footer">
                <p><?= Html::a('Seguir leyendo la noticia...',['noticias/noticia','id'=>$model->id],['class'=>'btn btn-block']) ?></p>
            </div>
          </div>
        </div>
           
        <?php }]); ?>
    </div>
</div>