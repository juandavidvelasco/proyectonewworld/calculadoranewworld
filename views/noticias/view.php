<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Noticias */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Noticias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="noticias-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-outline-danger',
            'data' => [
                'confirm' => 'Seguro que desea elminar?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Crear Nuevo Registro', ['create'], ['class' => 'btn btn-danger float-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'noticia:ntext',
            'title',
            [                      
                'label' => 'Banner',
                'value' => '@web/img/uploads/'.$model->banner_img,
                'format' => ['image',['width'=>'150','height'=>'100']]
            ],
            
            
        ],
    ]) ?>

</div>
