<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\helpers\Html;
use yii\widgets\ListView;

?> 
<div class="site-index fondo-ng">
    <div class="body-content ">
        
        <?= Html::img('@web/img/uploads/'.$datos[0]['banner_img'],['width'=>'100%']) ?>
        
        <h1 class="titulos text-center mb-4">
            <?= $datos[0]['title'] ?>
        </h1>
        
            
        <h2 class="titulos text-center mt-4 fondo-ng">
            <?= $datos[0]['noticia'] ?>
        </h2>
           
    </div>
</div>