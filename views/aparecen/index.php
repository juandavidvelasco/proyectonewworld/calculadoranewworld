<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Aparecen';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aparecen-index ">

    <h1>Gestion de Aparecen</h1>

    <p>
        <?= Html::a('Crear Registro', ['create'], ['class' => 'btn btn-danger']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [  
            [
                'attribute'=> 'Ingrediente',
                'value'=>'ingrediente.nombre'
           
            ],
            [
                'attribute'=> 'Zona',
                'value'=>'zonas.nombre'
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
