<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$cate = ArrayHelper::map($categorias,'id','nombre');
$zon = ArrayHelper::map($zonas,'id','nombre');
?>

<div class="aparecen-form">

    <?php $form = ActiveForm::begin(); ?>
    
     <?=    $form->field($model, 'id_ingrediente')->widget(Select2::classname(), [
                'data' => $cate,
                'options' => ['placeholder' => 'Seleccione la categoria'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?= $form->field($model, 'id_zonas')->widget(Select2::classname(), [
                'data' => $zon,
                'options' => ['placeholder' => 'Seleccione la zona'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
