<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aparecen */

$this->title = 'Modificar Registro';
$this->params['breadcrumbs'][] = ['label' => 'Aparecen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ingrediente->nombre, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="aparecen-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categorias' => $categorias,
        'zonas' => $zonas
    ]) ?>

</div>
