<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aparecen */

$this->title = 'Crear registro de la tabla Aparecen';
$this->params['breadcrumbs'][] = ['label' => 'Aparecen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aparecen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'categorias' => $categorias,
        'zonas' => $zonas
    ]) ?>

</div>
