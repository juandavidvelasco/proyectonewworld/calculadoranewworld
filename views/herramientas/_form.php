<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Herramientas */
/* @var $form yii\widgets\ActiveForm */
$prof = ArrayHelper::map($profesiones,'id','nombres');
?>

<div class="herramientas-form">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data']
    ]); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tier')->textInput() ?>

    <?= $form->field($model, 'velocidadMejora')->textInput() ?>

    <?=    $form->field($model, 'id_profesion')->widget(Select2::classname(), [
                'data' => $prof,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>
    
    <?= $form->field($model, 'image')->widget(\kartik\file\FileInput::classname(), [
        'options'=>['accept'=>'image/*'],
        'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
