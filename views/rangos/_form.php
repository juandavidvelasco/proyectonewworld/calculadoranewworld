<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

$prof = ArrayHelper::map($profesiones,'id','nombres');
?>

<div class="rangos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=    $form->field($model, 'id_profesion')->widget(Select2::classname(), [
                'data' => $prof,
                'options' => ['placeholder' => 'Seleccione uno'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
             ]);
     ?>

    <?= $form->field($model, 'nivel')->textInput() ?>

    <?= $form->field($model, 'exp_necesaria')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
