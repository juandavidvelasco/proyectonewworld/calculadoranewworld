<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rangos */

$this->title = $model->id_profesion;
$this->params['breadcrumbs'][] = ['label' => 'Rangos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="rangos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id_profesion' => $model->id_profesion,'nivel'=>$model->nivel], ['class' => 'btn btn-danger']) ?>
        <?= Html::a('Eliminar', ['delete', 'id_profesion' => $model->id_profesion,'nivel'=>$model->nivel], [
            'class' => 'btn btn-outline-danger',
            'data' => [
                'confirm' => 'Seguro que desea elminar?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Crear Nuevo Registro', ['create'], ['class' => 'btn btn-danger float-right']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'profesion.nombres',
            'nivel',
            'exp_necesaria',
        ],
    ]) ?>

</div>
