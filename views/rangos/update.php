<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rangos */

$this->title = 'Modificar Rangos: ' . $model->id_profesion;
$this->params['breadcrumbs'][] = ['label' => 'Rangos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_profesion, 'url' => ['view', 'id_profesion' => $model->id_profesion, 'nivel' => $model->nivel]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="rangos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'profesiones'=>$profesiones
    ]) ?>

</div>
