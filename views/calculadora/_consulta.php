<?php
$this->title = 'new world';
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;
use \yii\widgets\ListView;

?>

<div class="row justify-content-center justify-content-md-start barra ">

    <div class="col-7 align-self-center align-items-center  text-center">
        <h5>Nombre</h5>
    </div>
    <div class="col-2 align-self-center align-items-center text-center">
        <h5>Profesión</h5>
    </div>
    <div class="col-1 align-self-center align-items-center text-center">
        <h5>Tier</h5>
    </div>
    <div class="col-2 align-self-center align-items-center  text-center">
        <h5>Icono</h5>
    </div>

</div>

<?=
ListView::widget([
    'dataProvider' => $dataProvider,
    'layout' => "{items}",
    'itemView' => function($model) {
        ?>

        <div class="row align-self-center align-items-center  text-center" id="tabla">
        
            <div class="col-7 align-self-center align-items-center  text-center">
                 
               <?=  Html::tag('h4', $model['nombre'], ['class' => 'center mt-3','type'=>'button',
                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                    .done(function( data )
                        {
                            $("#consulta").html( data );
                        });'
                     ]);
                     
                ?>
               
            </div>
            <div class="col-2 align-self-center align-items-center text-center">
                <?=  Html::tag('h7', $model['nombre_profesiones'], ['class' => 'center mt-3','type'=>'button',
                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                    .done(function( data )
                        {
                            $("#consulta").html( data );
                        });'
                     ]);
                     
                ?>
            </div>
            <div class="col-1 align-self-center align-items-center  text-center">
                <?=  Html::tag('h3', $model['tier'], ['class' => 'center mt-3','type'=>'button',
                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                    .done(function( data )
                        {
                            $("#consulta").html( data );
                        });'
                     ]);
                     
                ?>
               </div>
            <div class="col-2 align-self-center align-items-center text-center">
                <?= Html::img('@web/img/thumbs/' . "thumb_" . $model['icono'], ['class' => 'prueba ', 'width' => '100%', 'type' => 'button', 'data-toggle' => 'modal',
                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                    .done(function( data )
                        {
                            $("#consulta").html( data );
                        });']) ?> 
            </div>

        </div>
        

    <?php }]); ?>

 
