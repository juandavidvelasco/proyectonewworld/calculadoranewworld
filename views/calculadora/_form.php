<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Url;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$tier = ArrayHelper::map($categorias, 'tier', 'tier');
$profesion = ArrayHelper::map($profesiones, 'nombres', 'nombres');
?>

<div > 
    <hr>
    <h5 class="text-center mt-2">Selccione por Profesión</h5>
    <?= Html::dropDownList('profesion','profesion',$profesion,['class'=>'form-control','prompt'=>'Seleccione La profesión','id'=>'profesion']) ?>
    <hr>
   <h5 class="text-center ">Seleccione por Tier</h5>
   <?= Html::dropDownList('tier','tier',$tier,['class'=>'form-control','prompt'=>'Seleccione El tier','id'=>'tier']) ?>
   <hr>
   <br> 
   <div class="row justify-content-center align-items-center mb-2">
        <?=
        Html::Button('Buscar', ['class' => 'btn btn-light text-center','id'=>'pr',
            'onclick' => '$.get( "'.Url::toRoute('calculadora/filtro').'",{ tier: $("#tier").val() , profesion: $("#profesion").val()})
            .done(function( data )
                {
                    $("#consulta").html( data );
                });'
        ]);
        ?>
    </div>
    
</div>
