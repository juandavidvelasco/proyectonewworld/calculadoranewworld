<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use yii\helpers\Html;
use \yii\widgets\ListView;
use yii\helpers\Url;
?>

<div class="row justify-content-center align-items-center text-center fondo-ng pb-3 ">
    <hr>
    <h2 class="text-center mt-2">Fabricación de <?= $objeto ?> </h2>
    <h2>Utilizando la Profesion <?= $rango[0]['prof'] ?></h2>
    <h2>Se busca producir  <?= $cantidad ?> del mismo elemento.</h2>
    <div class="col-sm-12 col-md-12 col-12 align-items-center">
    <h3> Experiencia generada en total: <?= $exp ?></h3>
    <h3> Experiencia necesaria para el siguiente rango: <?= $rango[0]['faltante'] ?>. Siguiente Rango: <?= $rango[0]['nivel'] ?> </h3>
    </div>
    
        <div class="col-sm-6 col-md-6 col-6 justify-">
            <h4>Cantidad a producir:</h4>
            <input id="canti" type="number" min="1" ondrop="return false;" onpaste="return false;" oninput="this.value = 
            !!this.value && Math.abs(this.value) >= 1 ? Math.abs(this.value) : 1" />
        </div>
        <div class="col-sm-6 col-md-6 col-6 ">
            
            <?=
                Html::Button('Calcular cantidad', ['class' => 'btn btn-danger',
                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$id.', nombre: "'.$nombre.'", cantidad : $("#canti").val()})
                    .done(function( data )
                        {
                            $("#consulta").html( data );
                        });'
                ]);
         

            ?>
            
            
            
        </div>
    
</div>
<?php
    if($dataProvider->getCount() > 0){
?>
    <div class="row justify-content-center align-items-center">
    <h3 class="text-center ">Ingredientes basicos</h3>
    <hr>
<?php 
    
    echo ListView::widget([
    'dataProvider' => $dataProvider,
    
    'layout' => "{items}",
    'itemView' => function($model) {
        $total = $model['cantidad'];
        
?>

    
    <div class="col-md-5 col-sm-6 col-lg-3">
        <div class="card" >
            <div class="row justify-content-center align-items-center mx-auto" type="button" data-toggle="modal" data-target="#ingrediente<?php echo $model['id'];?>" data-whatever="@getbootstrap">
               
                <?= Html::img('@web/img/iconos/'.$model['icono'],
                        ['class'=>'card-img-top']);  ?>
                
                
                    <div class="card-body" type="button" data-toggle="modal" data-target="#ingrediente<?php echo $model['id'];?>" data-whatever="@getbootstrap">
                            <h3 class="card-title text-center"><?= $model['nombre'] ?></h3>
                       <div class="card-text">
                       
                          
                       <p>Rareza: <?= $model['rareza'] ?></p>
                       <p >Tier: <?= $model['tier'] ?></p>
                       <p>Cantidad: <?=  $total ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Aqui empieza el modal de cada ingrediente -->
    
      <div class="modal fade mt-5" id="ingrediente<?php echo $model['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content ">
              <div class="modal-header fondo-ng">
                <h3 class="modal-title text-center" id="exampleModalLabel" align="center"> Vista de 
                    <?= $model['nombre']; ?> 
                </h3>
               
              </div>
              <div class="modal-body fondo-ng">
                <div class="row ">
                    <div class="col-md-5 col-sm-12 " align="center">
                        
                        <p class="fondo-ng">Rareza: <?= $model['rareza'] ?></p>
                        <p class="fondo-ng">Tier: <?= $model['tier'] ?></p>
                        <p class="fondo-ng"> Profesión Recolectado: <?= $model['prof'] ?></p>
                    </div>
                    
                    <?php if(strcasecmp($model['rareza'], 'poco común') == 0) {?>
                    <div class="col-md-6 col-sm-12 fondo-pc p-2" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div>               
                     <?php }elseif (strcasecmp($model['rareza'], 'raro') == 0) {?>
                    <div class="col-md-6 col-sm-12 fondo-raro" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div>  
                    <?php }elseif (strcasecmp($model['rareza'], 'epico') == 0) {?>   
                    <div class="col-md-6 col-sm-12 fondo-raro" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div> 
                    <?php }else{ ?>
                    <div class="col-md-6 col-sm-12 fondo-comun" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div>
                    <?php } ?>
                </div>
                  
                  <hr>
                    <!-- Inicio de las herramientas -->
                <?php
                    $contador = Yii::$app->db->createCommand("SELECT count(*) FROM ingredientes "
                            . "JOIN herramientas ON ingredientes.id_profesion = herramientas.id_profesion where ingredientes.id = ".$model['id']."")->queryScalar();
                    if($contador > 0){ 
                ?>
                <div class="row pt-2">
                    <h3 class="text-center">Herramientas utilizables para este ingrediente.</h3>
                    <?php 
                        echo ListView::widget([
                        'dataProvider' => new \yii\data\SqlDataProvider(['sql' => 'SELECT herramientas.nombre AS herramienta, herramientas.tier as tier,
                                                                herramientas.velocidadMejora as mejora,
                                                                herramientas.icono as icono FROM ingredientes
                                                                JOIN herramientas ON ingredientes.id_profesion = herramientas.id_profesion where ingredientes.id = '.$model['id'].'']),

                        'layout' => "{items}",
                        'itemView' => function($model) {

                    ?>  
                    
                    <div class="col-4 pt-2 pb-2">
                       
                        <div class="card">
                            <?= Html::img('@web/img/iconos/'.$model['icono'],['class'=>'card-img', 'height'=> '70%']) ?> 

                            <div class="card-body ">
                              <h5 class="card-title">  <?= $model['herramienta'] ?> </h5>

                              <div class="card-text"> 
                                  <h4> Mejora: <?= $model['mejora'] ?></h4>
                                  <h4> Tier: <?= $model['tier'] ?></h4>
                              
                              </div>

                            </div>
                            
                        </div>
                    </div>  
                    
                    <?php }]);?>

                </div>
                <?php }?>
                    <!-- Fin de las herramientas -->
                    
                    
                <div class="row">
                    <!-- Inicio del carrusel con el mapa de cada ingrediente -->
                    <?php
                        $contador = Yii::$app->db->createCommand("SELECT count(*) FROM aparecen where aparecen.id_ingrediente=".$model['id']."")->queryScalar();
                    if($contador>0){ ?>
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <h2>Mapa Global - Avanza para ver la ubicación del ingrediente</h2>
                                <?= Html::img('@web/img/mapas/mapa-general.png', ['class'=>'dblock w-100']) ?> 
                            </div>
                            <?php 
                            echo ListView::widget([
                            'dataProvider' => new \yii\data\SqlDataProvider(['sql' => 'SELECT zonas.nombre AS nombre,
                                    zonas.mapa AS mapa
                                    FROM zonas 
                                    JOIN aparecen ON zonas.id = aparecen.id_zonas
                                    WHERE aparecen.id_ingrediente = '.$model['id'].'']),

                            'layout' => "{items}",
                            'itemView' => function($model) {

                            ?>  
                                <div class="carousel-item">
                                    <h2 align="center"><?= $model['nombre'] ?></h2>
                                    <?= Html::img('@web/img/mapas/'.$model['mapa'], ['class'=>'dblock w-100']) ?> 
                                </div>

                            <?php }]);?>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <h2>No se encuentran mapas de este ingrediente.</h2>
                    <?php } ?>
                </div>
              <div class="modal-footer">
                            
              </div>
            </div>
          </div>
        </div>   
      </div>
        <!-- Aqui termina el modal -->

    
<?php }]); ?></div> <?php } ?>



<!-- Parte de los componentes -->



 <hr style="height: 4px">


<?php
    if($dataProvider2->getCount() > 0){
        
?>
<div class="row">
    <h3 class="text-center">Componentes utilizados</h3>
<?php
    echo ListView::widget([
        'dataProvider' => $dataProvider2,
        'layout' => "{items}",
        'itemView' => function($model) {
?>
    <div class="row justify-content-center align-items-center">
     <div class="col-md-5 col-sm-6 col-lg-3">
        <div class="card">
            <div class="row justify-content-center justify-content-md-start  mx-auto">
               
                <?= Html::img('@web/img/iconos/'.$model['icono'],['class'=>'card-img-top',]) ?>
                
                    
                    <div class="card-body cal">
                    <h3 class="card-title text-center"><?= $model['nombre'] ?></h3>
                    <div class="card-text">
                    
                    <p >Tier: <?= $model['tier'] ?></p>
                    <p>Cantidad: <?= $model['cantidad'] ?></p>
                    <p>Experiencia generada: <?= $model['exp'] ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
       
        <div class="col-md-5 col-sm-6 col-lg-3 ">
                <div class="card mt-md-5 mt-sm-1" >
                    
                    <div class="card-body cal">
                         <div class="card-text text-center mt-5">
                            <p>Fabricado en la profesion de <?= $model['profesion'] ?> con:</p>
                            
                         </div>
                    </div>
                </div>
        </div>
        <?=
         ListView::widget([
        'dataProvider' => new \yii\data\SqlDataProvider(['sql'=>'SELECT profesiones.nombres AS prof, ingredientes.id AS id,'
                        . '(procesan.cantidad_ingrediente * '.$model['cantidad'].'  )AS cantidad,ingredientes.nombre AS nombre,
                            ingredientes.rareza AS rareza,ingredientes.tier AS tier, ingredientes.icono AS icono
                            FROM componentes
                            JOIN procesan  ON componentes.id = procesan.id_componente 
                            JOIN ingredientes  ON procesan.id_ingrediente = ingredientes.id 
                            JOIN profesiones ON ingredientes.id_profesion = profesiones.id
                            WHERE componentes.id='.$model['id'].'']),
        'layout' => "{items}",
        'itemView' => function($model) {
        ?>

           <div class="col-md-5 col-sm-6 col-lg-3">
                <div class="card ">
                    <div class="row justify-content-center align-items-center mx-auto" type="button" data-toggle="modal" data-target="#ingrediente<?php echo $model['id'];?>" data-whatever="@getbootstrap">

                        <?= Html::img('@web/img/iconos/'.$model['icono'],['class'=>'card-img-top','width'=>'60%','height'=>'70%']) ?>

                            
                            <div class="card-body cal" type="button" data-toggle="modal" data-target="#ingrediente<?php echo $model['id'];?>" data-whatever="@getbootstrap">
                                <h3 class="card-title text-center mb-sm-3" ><?= $model['nombre'] ?></h3>
                                <div class="card-text">
                                <p>Rareza: <?= $model['rareza'] ?></p>
                                <p >Tier: <?= $model['tier'] ?></p>
                                <p>Cantidad: <?= $model['cantidad'] ?></p>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        <!-- Inicio del modal de cada ingrediente por componentes-->
            <div class="modal fade mt-5" id="ingrediente<?php echo $model['id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header fondo-ng">
                <h3 class="modal-title text-center" id="exampleModalLabel" align="center"> Vista de 
                    <?= $model['nombre']; ?> 
                </h3>
               
              </div>
              <div class="modal-body fondo-ng">
                <div class="row">
                    <div class="col-md-4 col-sm-12" align="center">
                        <p class="fondo-ng">Rareza: <?= $model['rareza'] ?></p>
                        <p class="fondo-ng">Tier: <?= $model['tier'] ?></p>
                        <p class="fondo-ng"> Profesión Recolectado: <?= $model['prof'] ?></p>
                    </div>
                   
                    <?php if(strcasecmp($model['rareza'], 'poco común') == 0) {?>
                    <div class="col-md-6 col-sm-12 fondo-pc" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div>               
                     <?php }elseif (strcasecmp($model['rareza'], 'raro') == 0) {?>
                    <div class="col-md-6 col-sm-12 fondo-raro" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div>  
                    <?php }elseif (strcasecmp($model['rareza'], 'epico') == 0) {?>   
                    <div class="col-md-6 col-sm-12 fondo-raro" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div> 
                    <?php }else{ ?>
                    <div class="col-md-6 col-sm-12 fondo-comun" align="center">
                        <?= Html::img('@web/img/iconos/'.$model['icono'], ['width'=> '100%','style'=>'filter: drop-shadow(1px 1px 5px white) brightness(1.5);']) ?> 
                    </div>
                    <?php } ?>
                </div>
                    <!-- Inicio de las herramientas -->
               <?php
                    $contador = Yii::$app->db->createCommand("SELECT count(*) FROM ingredientes "
                            . "JOIN herramientas ON ingredientes.id_profesion = herramientas.id_profesion where ingredientes.id = ".$model['id']."")->queryScalar();
                    if($contador > 0){ 
                ?>  
                <div class="row pt-2">
                    <h3 class="text-center">Herramientas utilizables para este ingrediente.</h3>
                    <?php 
                        echo ListView::widget([
                        'dataProvider' => new \yii\data\SqlDataProvider(['sql' => 'SELECT herramientas.nombre AS herramienta, herramientas.tier as tier,
                                                                herramientas.velocidadMejora as mejora,
                                                                herramientas.icono as icono FROM ingredientes
                                                                JOIN herramientas ON ingredientes.id_profesion = herramientas.id_profesion where ingredientes.id = '.$model['id'].'']),

                        'layout' => "{items}",
                        'itemView' => function($model) {

                    ?>  
                    
                    <div class="col-4 pt-2 pb-2">
                       
                        <div class="card">
                            <?= Html::img('@web/img/iconos/'.$model['icono'],['class'=>'card-img', 'height'=> '70%']) ?> 

                            <div class="card-body ">
                              <h5 class="card-title">  <?= $model['herramienta'] ?> </h5>

                              <div class="card-text"> 
                                  <h4> Mejora: <?= $model['mejora'] ?></h4>
                                  <h4> Tier: <?= $model['tier'] ?></h4>
                              
                              </div>

                            </div>
                            
                        </div>
                    </div>  
                    
                    <?php }]);?>

                </div>
                    <?php } ?>
                    <!-- Fin de las herramientas -->  
                
                <div class="row">
                    <!-- Inicio del carrusel con el mapa de cada ingrediente -->
                    <?php
                    $contador = Yii::$app->db->createCommand("SELECT count(*) FROM aparecen where aparecen.id_ingrediente=".$model['id']."")->queryScalar();
                    if($contador>0){ ?>
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <h2>Mapa Global - Avanza para ver la ubicación del ingrediente</h2>
                              <?= Html::img('@web/img/mapas/mapa-general.png', ['class'=>'dblock w-100']) ?> 
                            </div>
                            <?php 
                            echo ListView::widget([
                            'dataProvider' => new \yii\data\SqlDataProvider(['sql' => 'SELECT zonas.nombre AS nombre,
                                    zonas.mapa AS mapa
                                    FROM zonas 
                                    JOIN aparecen ON zonas.id = aparecen.id_zonas
                                    WHERE aparecen.id_ingrediente = '.$model['id'].'']),

                            'layout' => "{items}",
                            'itemView' => function($model) {

                            ?>  
                                <div class="carousel-item">
                                    <h2 align="center"><?= $model['nombre'] ?></h2>
                                    <?= Html::img('@web/img/mapas/'.$model['mapa'], ['class'=>'dblock w-100']) ?> 
                                </div>

                            <?php }]);?>
                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <h2>No se encuentran mapas de este ingrediente.</h2>
                    <?php } ?>
                </div>
              <div class="modal-footer">
                            
              </div>
            </div>
          </div>
        </div>   
      </div>
        <!-- Aqui termina el modal -->
        <?php }]); ?>
             
        
    <hr style="height: 4px">
   </div>

    <?php }]); ?> </div><?php } ?>

