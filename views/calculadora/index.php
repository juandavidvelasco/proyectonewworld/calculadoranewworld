<?php
$this->title = 'new world';

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
?>

<div class="site-index">

    <div class="container">
        <div class="row mt-2">
            <!-- Apartado del menu lateral o superior para la busqueda -->
            <div class="col-md-2 col-sm-12 p-1 fixed-blocks fondo-ng"><?= Yii::$app->controller->renderPartial('_form', ['categorias' => $categorias, 'profesiones' => $profesiones]); ?>
            </div>
            <!-- SECCIÓN CON LA TABLA DONDE SE ACTUALIZA CADA VEZ QUE SE BUSCA --> 
            <div class="col-md-9 col-sm-12 " id="consulta">

                <div class="row justify-content-center justify-content-md-start barra ">

                    <div class="col-7 align-self-center align-items-center  text-center">
                        <h5>Nombre</h5>
                    </div>
                    <div class="col-2 align-self-center align-items-center text-center">
                        <h5>Profesión</h5>
                    </div>
                    <div class="col-1 align-self-center align-items-center text-center">
                        <h5>Tier</h5>
                    </div>
                    <div class="col-2 align-self-center align-items-center  text-center">
                        <h5>Icono</h5>
                    </div>

                </div>

                <?=
                ListView::widget([
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}",
                    'itemView' => function($model) {
                        ?>

                        <div class="row align-self-center align-items-center  text-center" id="tabla">

                            <div class="col-7 align-self-center align-items-center  text-center">

                               <?=  Html::tag('h4', $model['nombre'], ['class' => 'center mt-3','type'=>'button',
                                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                                    .done(function( data )
                                        {
                                            $("#consulta").html( data );
                                        });'
                                     ]);

                                ?>

                            </div>
                            <div class="col-2 align-self-center align-items-center text-center">
                                <?=  Html::tag('h7', $model['nombre_profesiones'], ['class' => 'center mt-3','type'=>'button',
                                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                                    .done(function( data )
                                        {
                                            $("#consulta").html( data );
                                        });'
                                     ]);

                                ?>
                            </div>
                            <div class="col-1 align-self-center align-items-center  text-center">
                                <?=  Html::tag('h3', $model['tier'], ['class' => 'center mt-3','type'=>'button',
                                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                                    .done(function( data )
                                        {
                                            $("#consulta").html( data );
                                        });'
                                     ]);

                                ?>
                               </div>
                            <div class="col-2 align-self-center align-items-center text-center">
                                <?= Html::img('@web/img/thumbs/' . "thumb_" . $model['icono'], ['class' => 'prueba ', 'width' => '100%', 'type' => 'button', 'data-toggle' => 'modal',
                                    'onclick' => '$.get( "'.Url::toRoute('calculadora/modal').'",{ id: '.$model['id'].', nombre: "'.$model['nombre'].'", cantidad : 1  })
                                    .done(function( data )
                                        {
                                            $("#consulta").html( data );
                                        });']) ?> 
                            </div>

                        </div>


                    <?php }]); ?>


            </div>
            <div id="modalItem">
          
            </div>

        </div>
        
    </div>
</div>