<?php


namespace app\models;

use Yii;
use yii\web\UploadedFile;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/iconos/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/iconos/';

/**
 * This is the model class for table "ingredientes".
 *
 * @property int $id
 * @property int|null $id_profesion
 * @property int|null $id_categoria
 * @property int|null $experiencia
 * @property string|null $nombre
 * @property string|null $rareza
 * @property int|null $tier
 * @property string|null $icono
 *
 * @property Categorias $categoria
 * @property Procesan[] $procesans
 * @property Profesiones $profesion
 */


class Ingredientes extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ingredientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_profesion', 'id_categoria'], 'integer'],
            [['experiencia'],'integer','min'=>0,'max' => 1000],
            [['tier'],'integer','min'=>0,'max' => 5],
            [['nombre','rareza'], 'string','min' => 1, 'max' => 100],
            [['id_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['id_categoria' => 'id']],
            [['id_profesion'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::className(), 'targetAttribute' => ['id_profesion' => 'id']],
            [['id_profesion','id_categoria'],'required'],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_profesion' => 'Id Profesion',
            'id_categoria' => 'Id Categoria',
            'experiencia' => 'Experiencia',
            'rareza' => 'Rareza',
            'nombre' => 'Nombre Ingrediente',
            'tier' => 'Tier',
            'icono' => 'Icono'
        ];
    }

    /**
     * Gets query for [[Categoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria()
    {
        return $this->hasOne(Categorias::className(), ['id' => 'id_categoria']);
    }

    /**
     * Gets query for [[Procesans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProcesans()
    {
        return $this->hasMany(Procesan::className(), ['id_ingrediente' => 'id']);
    }

    /**
     * Gets query for [[Profesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }
        
    public function getImageFile() 
    {
        return isset($this->icono) ? Yii::$app->params['uploadPath'] . $this->icono : null;
    }
     public function getThumbsFile() 
    {
        return isset($this->icono) ? Yii::$app->basePath . '\\web\\img\\thumbs\\' . 'thumb_' . $this->icono : null;
    }
    
    public function getImageUrl() 
    { 
        $icono = isset($this->icono) ? $this->icono : 'default_user.jpg';
        return Yii::$app->params['uploadUrl'] . $icono;
    }
    
    public function uploadImage() {
        
        $image = UploadedFile::getInstance($this, 'image');
        if (empty($image)) {
            return false;
        }

        $tmp = explode(".",$image->name);
        $ext = end($tmp);

        $this->icono = Yii::$app->security->generateRandomString().".{$ext}";

        return $image;
    }
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->icono = null;

        return true;
    }
}
