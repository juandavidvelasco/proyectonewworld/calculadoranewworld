<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rangos".
 *
 * @property int $id_profesion
 * @property int $nivel
 * @property int|null $exp_necesaria
 *
 * @property Profesiones $profesion
 */
class Rangos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rangos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_profesion', 'nivel'], 'required'],
            [['id_profesion', 'nivel', 'exp_necesaria'], 'integer'],
            [['id_profesion', 'nivel'], 'unique', 'targetAttribute' => ['id_profesion', 'nivel']],
            [['id_profesion'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::className(), 'targetAttribute' => ['id_profesion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_profesion' => 'Id Profesion',
            'nivel' => 'Nivel',
            'exp_necesaria' => 'Exp Necesaria',
        ];
    }

    /**
     * Gets query for [[Profesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }
}
