<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/iconos/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/iconos/';

/**
 * This is the model class for table "herramientas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $tier
 * @property float|null $velocidadMejora
 * @property int|null $id_profesion
 *
 * @property Profesiones $profesion
 */
class Herramientas extends \yii\db\ActiveRecord
{
    public $image;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'herramientas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tier', 'id_profesion'], 'integer'],
            [['velocidadMejora'], 'number'],
            [['nombre'], 'string', 'max' => 50],
            [['id_profesion'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::className(), 'targetAttribute' => ['id_profesion' => 'id']],
            [['id_profesion'],'required'],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'tier' => 'Tier',
            'velocidadMejora' => 'Velocidad Mejora',
            'id_profesion' => 'Id Profesion',
            'icono' => 'Icono'
        ];
    }

    /**
     * Gets query for [[Profesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }
    
    // Inicia la carga de la imagen
    public function getImageFile() 
    {
        return isset($this->icono) ? Yii::$app->params['uploadPath'] . $this->icono : null;
    }
     public function getThumbsFile() 
    {
        return isset($this->icono) ? Yii::$app->basePath . '\\web\\img\\thumbs\\' . 'thumb_' . $this->icono : null;
    }
    
    public function getImageUrl() 
    { 
        $icono = isset($this->icono) ? $this->icono : 'default_user.jpg';
        return Yii::$app->params['uploadUrl'] . $icono;
    }
    
    public function uploadImage() {
        
        $image = UploadedFile::getInstance($this, 'image');
        if (empty($image)) {
            return false;
        }

        $tmp = explode(".",$image->name);
        $ext = end($tmp);

        $this->icono = Yii::$app->security->generateRandomString().".{$ext}";

        return $image;
    }
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->icono = null;

        return true;
    }
}
