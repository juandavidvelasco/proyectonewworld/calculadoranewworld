<?php

namespace app\models;
use app\models\Profesiones;
use Yii;

/**
 * This is the model class for table "fabrican".
 *
 * @property int $id
 * @property int|null $id_utilizable
 * @property int|null $id_componente
 * @property int|null $id_profesion
 * @property int|null $cantidad_componente
 *
 * @property Componentes $componente
 * @property Profesiones $profesion
 * @property Utilizables $utilizable
 */
class Fabrican extends \yii\db\ActiveRecord
{
    public $nombre_profesiones;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fabrican';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id_utilizable','unique', 'targetAttribute' => ['id_utilizable', 'id_componente']],
            [['id_utilizable', 'id_componente', 'id_profesion'], 'integer'],
            [['cantidad_componente'],'integer','min'=>1,'max'=>30],
            [['id_componente'], 'exist', 'skipOnError' => true, 'targetClass' => Componentes::className(), 'targetAttribute' => ['id_componente' => 'id']],
            [['id_profesion'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::className(), 'targetAttribute' => ['id_profesion' => 'id']],
            [['id_utilizable'], 'exist', 'skipOnError' => true, 'targetClass' => Utilizables::className(), 'targetAttribute' => ['id_utilizable' => 'id']],
            [['id_utilizable', 'id_componente', 'id_profesion'],'required']
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_utilizable' => 'Id Utilizable',
            'id_componente' => 'Id Componente',
            'id_profesion' => 'Id Profesion',
            'cantidad_componente' => 'Cantidad Componente',
        ];
    }

    /**
     * Gets query for [[Componente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componentes::className(), ['id' => 'id_componente']);
    }

    /**
     * Gets query for [[Profesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }

    /**
     * Gets query for [[Utilizable]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizable()
    {
        return $this->hasOne(Utilizables::className(), ['id' => 'id_utilizable']);
    }
}
