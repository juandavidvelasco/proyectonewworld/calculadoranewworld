<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/mapas/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/mapas/';
/**
 * This is the model class for table "zonas".
 *
 * @property int $id
 * @property string|null $nombre
 * @property string|null $tipo_zona
 *
 * @property Aparecen[] $aparecens
 */
class Zonas extends \yii\db\ActiveRecord
{
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 100],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'mapa' => 'Mapa',
        ];
    }

    /**
     * Gets query for [[Aparecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAparecens()
    {
        return $this->hasMany(Aparecen::className(), ['id_zonas' => 'id']);
    }
    public function getImageFile() 
    {
        return isset($this->mapa) ? Yii::$app->params['uploadPath'] . $this->mapa : null;
    }
    
    public function getImageUrl() 
    { 
        $$mapa = isset($this->mapa) ? $this->mapa : 'default_mapa.jpg';
        return Yii::$app->params['uploadUrl'] . $$mapa;
    }
    
    public function uploadImage() {
        
        $image = UploadedFile::getInstance($this, 'image');
        if (empty($image)) {
            return false;
        }

        $tmp = explode(".",$image->name);
        $ext = end($tmp);

        $this->mapa = $image->name;

        return $image;
    }
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->mapa = null;

        return true;
    }
    
    
}
