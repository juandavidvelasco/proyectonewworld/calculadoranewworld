<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fabrican_ingrediente".
 *
 * @property int $id
 * @property int|null $id_utilizable
 * @property int|null $id_ingrediente
 * @property int|null $id_profesion
 * @property int|null $cantidad_componente
 *
 * @property Ingredientes $ingrediente
 * @property Profesiones $profesion
 * @property Utilizables $utilizable
 */
class FabricanIngrediente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fabrican_ingrediente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id_utilizable','unique', 'targetAttribute' => ['id_utilizable', 'id_ingrediente']],
            [['id_utilizable', 'id_ingrediente', 'id_profesion', 'cantidad_componente'], 'integer'],
            [['cantidad_componente'],'integer','min'=>1,'max'=>30],
            [['id_profesion'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::className(), 'targetAttribute' => ['id_profesion' => 'id']],
            [['id_utilizable'], 'exist', 'skipOnError' => true, 'targetClass' => Utilizables::className(), 'targetAttribute' => ['id_utilizable' => 'id']],
            [['id_ingrediente'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredientes::className(), 'targetAttribute' => ['id_ingrediente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_utilizable' => 'Id Utilizable',
            'id_ingrediente' => 'Id Ingrediente',
            'id_profesion' => 'Id Profesion',
            'cantidad_componente' => 'Cantidad Componente',
        ];
    }

    /**
     * Gets query for [[Ingrediente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngrediente()
    {
        return $this->hasOne(Ingredientes::className(), ['id' => 'id_ingrediente']);
    }

    /**
     * Gets query for [[Profesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }

    /**
     * Gets query for [[Utilizable]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizable()
    {
        return $this->hasOne(Utilizables::className(), ['id' => 'id_utilizable']);
    }
}
