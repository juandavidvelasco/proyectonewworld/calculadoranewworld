<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesiones".
 *
 * @property int $id
 * @property string|null $nombres
 *
 * @property Fabrican[] $fabricans
 * @property Herramientas[] $herramientas
 * @property Ingredientes[] $ingredientes
 * @property Procesan[] $procesans
 * @property Rangos[] $rangos
 */
class Profesiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombres'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombres' => 'Nombre Profesión',
        ];
    }

    /**
     * Gets query for [[Fabricans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFabricans()
    {
        return $this->hasMany(Fabrican::className(), ['id_profesion' => 'id']);
    }

    /**
     * Gets query for [[Herramientas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHerramientas()
    {
        return $this->hasMany(Herramientas::className(), ['id_profesion' => 'id']);
    }

    /**
     * Gets query for [[Ingredientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientes()
    {
        return $this->hasMany(Ingredientes::className(), ['id_profesion' => 'id']);
    }

    /**
     * Gets query for [[Procesans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProcesans()
    {
        return $this->hasMany(Procesan::className(), ['id_profesion' => 'id']);
    }

    /**
     * Gets query for [[Rangos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRangos()
    {
        return $this->hasMany(Rangos::className(), ['id_profesion' => 'id']);
    }
}
