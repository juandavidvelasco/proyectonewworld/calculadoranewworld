<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web\UploadedFile;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/uploads/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/uploads/';
/**
 * This is the model class for table "noticias".
 *
 * @property int $id
 * @property string|null $noticia
 * @property string|null $title
 * @property string|null $banner_img
 * @property string|null $img_1
 * @property string|null $nombre_banner
 * @property string|null $banner_img
 * @property string|null $file_banner
 */
class Noticias extends \yii\db\ActiveRecord
{
    public $image;
   
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['noticia'], 'string'],
            [['title'], 'string', 'max' => 80],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'noticia' => 'Noticia',
            'title' => 'Title',
            'banner_img' => 'Banner Img',
        ];
    }
    
    public function getImageFile() 
    {
        return isset($this->banner_img) ? Yii::$app->params['uploadPath'] . $this->banner_img : null;
    }
    
    public function getImageUrl() 
    {
        
        $avatar = isset($this->banner_img) ? $this->banner_img : 'default_user.jpg';
        return Yii::$app->params['uploadUrl'] . $avatar;
    }
    
    public function uploadImage() {
        
        $image = UploadedFile::getInstance($this, 'image');
        if (empty($image)) {
            return false;
        }

        $tmp = explode(".",$image->name);
        $ext = end($tmp);

        $this->banner_img = Yii::$app->security->generateRandomString().".{$ext}";

        return $image;
    }
    
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->banner_img = null;

        return true;
    }
}
