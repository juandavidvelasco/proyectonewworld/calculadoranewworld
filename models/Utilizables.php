<?php

namespace app\models;
use Yii;
use yii\web\UploadedFile;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/iconos/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/iconos/';
/**
 * This is the model class for table "utilizables".
 *
 * @property int $id
 * @property int|null $tier
 * @property int|null $exp_otorgada
 * @property string|null $nombre
 * @property string|null $estadisticas
 *
 * @property Componentes[] $componentes
 * @property Fabrican[] $fabricans
 */
class Utilizables extends \yii\db\ActiveRecord
{
     public $nombre_utilizables;
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tier', 'exp_otorgada'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['estadisticas'], 'string', 'max' => 500],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tier' => 'Tier',
            'exp_otorgada' => 'Exp Otorgada',
            'nombre' => 'Nombre Utilizable',
            'estadisticas' => 'Estadisticas',
            'icono' => 'Icono'
        ];
    }

    /**
     * Gets query for [[Componentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponentes()
    {
        return $this->hasMany(Componentes::className(), ['id' => 'id_componente'])->viaTable('fabrican', ['id_utilizable' => 'id']);
    }

    /**
     * Gets query for [[Fabricans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFabricans()
    {
        return $this->hasMany(Fabrican::className(), ['id_utilizable' => 'id']);
    }
    
    public function getImageFile() 
    {
        return isset($this->icono) ? Yii::$app->params['uploadPath'] . $this->icono : null;
    }
    
     public function getThumbsFile() 
    {
        return isset($this->icono) ? Yii::$app->basePath . '\\web\\img\\thumbs\\' . 'thumb_' . $this->icono : null;
    }
    
    public function getImageUrl() 
    { 
        $icono = isset($this->icono) ? $this->icono : 'default_user.jpg';
        return Yii::$app->params['uploadUrl'] . $icono;
    }
    
    public function uploadImage() {
        
        $image = UploadedFile::getInstance($this, 'image');
        if (empty($image)) {
            return false;
        }

        $tmp = explode(".",$image->name);
        $ext = end($tmp);

        $this->icono = Yii::$app->security->generateRandomString().".{$ext}";

        return $image;
    }
    public function deleteImage() {
        $file = $this->getImageFile();

        // check if file exists on server
        if (empty($file) || !file_exists($file)) {
            return false;
        }

        // check if uploaded file can be deleted on server
        if (!unlink($file)) {
            return false;
        }

        // if deletion successful, reset your file attributes
        $this->icono = null;

        return true;
    }
}
