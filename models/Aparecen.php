<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aparecen".
 *
 * @property int $id
 * @property int|null $id_ingrediente
 * @property int|null $id_zonas
 *
 * @property Ingredientes $ingrediente
 * @property Zonas $zonas
 */
class Aparecen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aparecen';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_ingrediente', 'id_zonas'], 'integer'],
            [['id_ingrediente'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredientes::className(), 'targetAttribute' => ['id_ingrediente' => 'id']],
            [['id_zonas'], 'exist', 'skipOnError' => true, 'targetClass' => Zonas::className(), 'targetAttribute' => ['id_zonas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_ingrediente' => 'Id Ingrediente',
            'id_zonas' => 'Id Zonas',
        ];
    }

    /**
     * Gets query for [[Ingrediente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngrediente()
    {
        return $this->hasOne(Ingredientes::className(), ['id' => 'id_ingrediente']);
    }

    /**
     * Gets query for [[Zonas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getZonas()
    {
        return $this->hasOne(Zonas::className(), ['id' => 'id_zonas']);
    }
}
