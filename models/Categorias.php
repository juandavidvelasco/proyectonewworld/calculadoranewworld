<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property int $id
 * @property string|null $tipo
 *
 * @property Aparecen[] $aparecens
 * @property Ingredientes[] $ingredientes
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo'], 'string', 'min'=>1 , 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo de categoria',
        ];
    }

    /**
     * Gets query for [[Aparecens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAparecens()
    {
        return $this->hasMany(Aparecen::className(), ['id_categoria' => 'id']);
    }

    /**
     * Gets query for [[Ingredientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientes()
    {
        return $this->hasMany(Ingredientes::className(), ['id_categoria' => 'id']);
    }
}
