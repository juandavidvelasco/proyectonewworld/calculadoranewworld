<?php

namespace app\models;
use app\models\Profesiones;
use Yii;

use yii\web\UploadedFile;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '\\web\\img\\iconos\\';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '\\web\\img\\iconos\\';
/**
 * This is the model class for table "componentes".
 *
 * @property int $id
 * @property int|null $tier
 * @property int|null $exp_otorgada
 * @property string|null $nombre
 *
 * @property Fabrican[] $fabricans
 * @property Ingredientes[] $ingredientes
 * @property Procesan[] $procesans
 * @property Utilizables[] $utilizables
 */
class Componentes extends \yii\db\ActiveRecord
{
    public $nombre_profesiones;
    public $image;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'componentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['tier','integer','min'=>0,'max'=>5],
            ['exp_otorgada','integer','min'=>0,'max'=> 1000],
            [['nombre'], 'string','min'=> 2 , 'max' => 50],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tier' => 'Tier',
            'exp_otorgada' => 'Exp Otorgada',
            'nombre' => 'Nombre del componente',
            'icono' => 'Icono'
        ];
    }

    /**
     * Gets query for [[Fabricans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFabricans()
    {
        return $this->hasMany(Fabrican::className(), ['id_componente' => 'id']);
    }

    /**
     * Gets query for [[Ingredientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIngredientes()
    {
        return $this->hasMany(Ingredientes::className(), ['id' => 'id_ingrediente'])->viaTable('procesan', ['id_componente' => 'id']);
    }

    /**
     * Gets query for [[Procesans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProcesans()
    {
        return $this->hasMany(Procesan::className(), ['id_componente' => 'id']);
    }

    /**
     * Gets query for [[Utilizables]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizables()
    {
        return $this->hasMany(Utilizables::className(), ['id' => 'id_utilizable'])->viaTable('fabrican', ['id_componente' => 'id']);
    }
    
     public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }
    
    public function getImageFile() 
    {
        return isset($this->icono) ? Yii::$app->params['uploadPath'] . $this->icono : null;
    }
     public function getThumbsFile() 
    {
        return isset($this->icono) ? Yii::$app->basePath . '\\web\\img\\thumbs\\' . 'thumb_' . $this->icono : null;
    }
    
    public function getImageUrl() 
    { 
        $icono = isset($this->icono) ? $this->icono : 'default.jpg';
        return Yii::$app->params['uploadUrl'] . $icono;
    }
    
    public function uploadImage() {
        
        $image = UploadedFile::getInstance($this, 'image');
        if (empty($image)) {
            return false;
        }

        $tmp = explode(".",$image->name);
        $ext = end($tmp);

        $this->icono = Yii::$app->security->generateRandomString().".{$ext}";

        return $image;
    }
    public function deleteImage() {
        $file = $this->getImageFile();
        $thumbs = $this->getThumbsFile();
        
        if (empty($file) || !file_exists($file)) {
            return false;
        }
       
        
        if (!unlink($file) && unlink($thumbs) ) {
            return false;
        }
        
        
        
        $this->icono = null;

        return true;
    }
}
