<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "procesan_componentes".
 *
 * @property int $id
 * @property int|null $id_componente
 * @property int|null $id_componente_utilizado
 * @property int|null $id_profesion
 * @property int|null $cantidad_ingrediente
 *
 * @property Componentes $componente
 * @property Profesiones $profesion
 */
class ProcesanComponentes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'procesan_componentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id_componente','unique', 'targetAttribute' => ['id_componente', 'id_componente_utilizado']],
            [['id_componente', 'id_componente_utilizado', 'id_profesion'], 'integer'],
            [['cantidad_ingrediente'],'integer','min'=>1,'max'=>30],
            [['id_componente'], 'exist', 'skipOnError' => true, 'targetClass' => Componentes::className(), 'targetAttribute' => ['id_componente' => 'id']],
            [['id_profesion'], 'exist', 'skipOnError' => true, 'targetClass' => Profesiones::className(), 'targetAttribute' => ['id_profesion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_componente' => 'Id Componente',
            'id_componente_utilizado' => 'Id Componente Utilizado',
            'id_profesion' => 'Id Profesion',
            'cantidad_ingrediente' => 'Cantidad Ingrediente',
        ];
    }

    /**
     * Gets query for [[Componente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComponente()
    {
        return $this->hasOne(Componentes::className(), ['id' => 'id_componente']);
    }

    /**
     * Gets query for [[Profesion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProfesion()
    {
        return $this->hasOne(Profesiones::className(), ['id' => 'id_profesion']);
    }
}
