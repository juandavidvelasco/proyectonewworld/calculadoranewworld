<?php

namespace app\controllers;
use Yii;
use app\models\Herramientas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\imagine\Image;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/iconos/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/iconos/';

/**
 * HerramientasController implements the CRUD actions for Herramientas model.
 */
class HerramientasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Herramientas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Herramientas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Herramientas model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   public function actionCreate()
    {
       $profesiones = Yii::$app->db->createCommand("select id,nombres from profesiones")->queryAll();
        $model = new Herramientas();
        if ($model->load(Yii::$app->request->post())) {
            // process uploaded image file instance
            $image = $model->uploadImage();
            if($image != null){
                if ($model->save()) {
                    // upload only if valid uploaded file instance found

                        $path = $model->getImageFile();
                        $image->saveAs($path);
                        Image::thumbnail(Yii::$app->basePath . '/web/img/iconos/'.$model->icono, 160, 90)->save(Yii::$app->basePath . '/web/img/thumbs/'.'thumb_' . $model->icono, ['quality' => 90]);

                    return $this->redirect(['view', 'id'=>$model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'No se pudo crear el registro, recuerde insertar el registro.');
            }
        }

        return $this->render('create', [
            'model' => $model,
            'profesiones' => $profesiones, 
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFile = $model->getImageFile();
        $oldIcono = $model->icono;
        $oldthumbs = $model->getThumbsFile();
        $profesiones = Yii::$app->db->createCommand("select id,nombres from profesiones")->queryAll();

        if ($model->load(Yii::$app->request->post())) {
            // process uploaded image file instance
            $image = $model->uploadImage();

            // revert back if no valid file instance uploaded
            if ($image === false) {
                $model->icono = $oldIcono;
            }
            
                if ($model->save()) {
                    if ($image !== false && unlink($oldFile) && unlink($oldthumbs)) { // delete old and overwrite
                // upload only if valid uploaded file instance found
                
                    $path = $model->getImageFile();
                    
                    $image->saveAs($path);
                    Image::thumbnail(Yii::$app->basePath . '/web/img/iconos/'.$model->icono, 160, 90)->save(Yii::$app->basePath . '/web/img/thumbs/'.'thumb_' . $model->icono, ['quality' => 90]);
                }
                return $this->redirect(['view', 'id'=>$model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'No se pudo crear el registro o no se ha modificado la imagen, recuerde insertar el registro.');
            }
        }
        return $this->render('update', [
            'model' => $model,
            'profesiones' => $profesiones, 
        ]);
    }

    /**
     * Deletes an existing Herramientas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            if (!$model->deleteImage()) {
                Yii::$app->session->setFlash('error', 'No se encontro la imagen para eliminar');
            }
        }
        return $this->redirect(['index']);
    }
    
    /**
     * Finds the Herramientas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Herramientas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Herramientas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
