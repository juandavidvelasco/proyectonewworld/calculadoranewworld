<?php

namespace app\controllers;
use Yii;
use yii\data\ActiveDataProvider;

use app\models\Aparecen;
use app\models\Ingredientes;
use app\models\Categorias;
use app\models\Fabrican;
use app\models\Herramientas;
use app\models\Componentes;
use app\models\Profesiones;

class CalculadoraController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $categorias = Yii::$app->db->createCommand("select tier from Componentes union select tier from utilizables ")->queryAll();
        $profesiones = Yii::$app->db->createCommand("SELECT nombres FROM profesiones p INNER JOIN fabrican f ON f.id_profesion = p.id
        union
        SELECT nombres FROM profesiones p INNER JOIN procesan f ON f.id_profesion = p.id; ")->queryAll();
        
         $tablaA = (new \yii\db\Query())
                        ->select(['componentes.id AS id','componentes.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' , 'tier', 'icono'])
                        ->from('procesan')
                        ->innerJoin('componentes', 'componentes.id = procesan.id_componente')
                        ->innerJoin('profesiones', 'profesiones.id = procesan.id_profesion')
                        ->where(['!=', 'componentes.nombre', ''])
                        
                    ;
        $tablaB = (new \yii\db\Query())
              ->select(['utilizables.id AS id','utilizables.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' ,'tier', 'icono'])
              ->from('fabrican')
              ->innerJoin('utilizables', 'utilizables.id = fabrican.id_utilizable')
              ->innerJoin('profesiones', 'profesiones.id = fabrican.id_profesion')
              ->where(['!=', 'utilizables.nombre', ''])

            ;
         $tablaA->union($tablaB);

            $query = Componentes::find()->select('*')->from(['random_name' => $tablaA]);
           
            $dataProvider = new ActiveDataProvider([
               'query' => $query,
               'pagination' => [
                  'pageSize' => 20,
                ],
               'sort'=> ['defaultOrder' => ['nombre' => SORT_ASC]],
            ]); 
        
        return $this->render('index',[
           'categorias'=>$categorias,
           'profesiones'=>$profesiones,
           'dataProvider'=>$dataProvider
        ]);
    }
    
    public function actionFiltro($tier,$profesion){
       
            
            if(($tier != null || $tier != "")&&($profesion != null || $profesion != "")){
                $tablaA = (new \yii\db\Query())
                        ->select(['componentes.id AS id','componentes.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' , 'tier', 'icono'])
                        ->from('procesan')
                        ->innerJoin('componentes', 'componentes.id = procesan.id_componente')
                        ->innerJoin('profesiones', 'profesiones.id = procesan.id_profesion')
                        ->where(['!=', 'componentes.nombre', ''])
                        ->andWhere('tier = ' .$tier)
                        ->andWhere('profesiones.nombres = '."'".$profesion."'")
                    ;
                $tablaB = (new \yii\db\Query())
                      ->select(['utilizables.id AS id','utilizables.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' ,'tier', 'icono'])
                      ->from('fabrican')
                      ->innerJoin('utilizables', 'utilizables.id = fabrican.id_utilizable')
                      ->innerJoin('profesiones', 'profesiones.id = fabrican.id_profesion')
                      ->where(['!=', 'utilizables.nombre', ''])
                      ->andWhere('tier = ' .$tier)
                      ->andWhere('profesiones.nombres = '."'".$profesion."'")
                    ;
            }else if($tier != null || $tier != ""){
                $tablaA = (new \yii\db\Query())
                        ->select(['componentes.id AS id','componentes.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' , 'tier', 'icono'])
                        ->from('procesan')
                        ->innerJoin('componentes', 'componentes.id = procesan.id_componente')
                        ->innerJoin('profesiones', 'profesiones.id = procesan.id_profesion')
                        ->where(['!=', 'componentes.nombre', ''])
                        ->andWhere('tier = ' .$tier);
                $tablaB = (new \yii\db\Query())
                      ->select(['utilizables.id AS id','utilizables.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' ,'tier', 'icono'])
                      ->from('fabrican')
                      ->innerJoin('utilizables', 'utilizables.id = fabrican.id_utilizable')
                      ->innerJoin('profesiones', 'profesiones.id = fabrican.id_profesion')
                      ->where(['!=', 'utilizables.nombre', ''])
                      ->andWhere('tier = ' .$tier);
            }else if($profesion != null || $profesion != ""){
                $tablaA = (new \yii\db\Query())
                        ->select(['componentes.id AS id','componentes.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' , 'tier', 'icono'])
                        ->from('procesan')
                        ->innerJoin('componentes', 'componentes.id = procesan.id_componente')
                        ->innerJoin('profesiones', 'profesiones.id = procesan.id_profesion')
                        ->where(['!=', 'componentes.nombre', ''])
                        
                        ->andWhere('profesiones.nombres = '."'".$profesion."'")
                    ;
                $tablaB = (new \yii\db\Query())
                      ->select(['utilizables.id AS id','utilizables.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' ,'tier', 'icono'])
                      ->from('fabrican')
                      ->innerJoin('utilizables', 'utilizables.id = fabrican.id_utilizable')
                      ->innerJoin('profesiones', 'profesiones.id = fabrican.id_profesion')
                      ->where(['!=', 'utilizables.nombre', ''])
                      
                      ->andWhere('profesiones.nombres = '."'".$profesion."'")
                    ;
            }else if(($tier == null || $tier == "")&&($profesion == null || $profesion == "")){
                $tablaA = (new \yii\db\Query())
                        ->select(['componentes.id AS id','componentes.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' , 'tier', 'icono'])
                        ->from('procesan')
                        ->innerJoin('componentes', 'componentes.id = procesan.id_componente')
                        ->innerJoin('profesiones', 'profesiones.id = procesan.id_profesion')
                        ->where(['!=', 'componentes.nombre', ''])
                        
                    ;
                $tablaB = (new \yii\db\Query())
                      ->select(['utilizables.id AS id','utilizables.nombre AS nombre', 'profesiones.nombres AS nombre_profesiones' ,'tier', 'icono'])
                      ->from('fabrican')
                      ->innerJoin('utilizables', 'utilizables.id = fabrican.id_utilizable')
                      ->innerJoin('profesiones', 'profesiones.id = fabrican.id_profesion')
                      ->where(['!=', 'utilizables.nombre', ''])
                                            
                    ;
            }
            

            $tablaA->union($tablaB);

            $query = Componentes::find()->select('*')->from(['random_name' => $tablaA]);
           
            $dataProvider = new ActiveDataProvider([
               'query' => $query,
               'pagination' => [
                  'pageSize' => 20,
                ],
               'sort'=> ['defaultOrder' => ['nombre' => SORT_ASC]],
            ]); 
            
            
            
        $count = $dataProvider->getCount();
        return $this->renderPartial('_consulta',[
            'dataProvider'=>$dataProvider,
            'count'=>$count,
        ]);
    }
    
    public function actionModal($id, $nombre, $cantidad)
    {
       // forma de buscar acorde al id y al nombre para asegurar que no se repitan.
       $flag = Yii::$app->db->createCommand("SELECT count(*) FROM componentes where id='$id' and nombre like '$nombre'")->queryScalar();
       
       if($flag > 0){
            
            $dataProvider = new \yii\data\SqlDataProvider([
               'sql' => 'SELECT profesiones.nombres AS prof,ingredientes.id AS id,(procesan.cantidad_ingrediente* '.$cantidad.') AS cantidad,ingredientes.nombre AS nombre,
                   ingredientes.rareza AS rareza,ingredientes.tier AS tier, ingredientes.icono AS icono
                        FROM componentes  JOIN procesan  ON componentes.id = procesan.id_componente 
                        JOIN ingredientes  ON procesan.id_ingrediente = ingredientes.id 
                        JOIN profesiones ON ingredientes.id_profesion = profesiones.id
                        WHERE componentes.id='.$id.''
            ]);
            $dataProvider2 = new \yii\data\SqlDataProvider([
                    'sql' => 'SELECT componentes.id as id,(procesan_componentes.cantidad_ingrediente * '.$cantidad.') AS cantidad, 
                        componentes.nombre AS nombre, componentes.tier AS tier, componentes.icono AS icono,
                        profesiones.nombres AS profesion, (componentes.exp_otorgada* '.$cantidad.') AS exp
                        FROM procesan_componentes 
                        JOIN componentes ON procesan_componentes.id_componente_utilizado = componentes.id
                        JOIN profesiones ON procesan_componentes.id_profesion = profesiones.id
                        WHERE procesan_componentes.id_componente = '.$id.''
             ]);
             $exp = Yii::$app->db->createCommand("SELECT exp_otorgada * $cantidad FROM componentes where id='$id' and nombre like '$nombre'")->queryScalar();
            $rango = Yii::$app->db->createCommand("SELECT min(rangos.exp_necesaria)-$exp As faltante, nivel,profesiones.nombres As prof FROM rangos"
                         . " join procesan on procesan.id_componente='.$id.'"
                         . " join profesiones ON procesan.id_profesion = profesiones.id"
                         . " where exp_necesaria > $exp")->queryAll();
            }
       else {
            $dataProvider2 = new \yii\data\SqlDataProvider([
               'sql' => 'select distinct componentes.id as id,(fabrican.cantidad_componente * '.$cantidad.') AS cantidad, componentes.nombre AS nombre,componentes.tier AS tier, componentes.icono AS icono 
                        , profesiones.nombres AS profesion , (componentes.exp_otorgada* '.$cantidad.') AS exp                       
                        FROM fabrican 
                        JOIN componentes ON fabrican.id_componente = componentes.id
                        JOIN procesan ON procesan.id_componente = componentes.id
                        JOIN profesiones ON procesan.id_profesion = profesiones.id
                        WHERE fabrican.id_utilizable='.$id.'
                        '
            ]);
            $dataProvider = new \yii\data\SqlDataProvider([
                'sql' => 'SELECT profesiones.nombres AS prof,ingredientes.id AS id,(fabrican_ingrediente.cantidad_componente * '.$cantidad.') AS cantidad ,
                    ingredientes.nombre AS nombre,ingredientes.rareza AS rareza, ingredientes.tier AS tier, ingredientes.icono AS icono
                          FROM  fabrican_ingrediente
                          JOIN ingredientes ON fabrican_ingrediente.id_ingrediente = ingredientes.id
                          JOIN profesiones ON ingredientes.id_profesion = profesiones.id
                          WHERE fabrican_ingrediente.id_utilizable = '.$id.''
             ]);
           
            $exp = Yii::$app->db->createCommand("SELECT (exp_otorgada * $cantidad) FROM utilizables where id='$id'")->queryScalar();
            $rango = Yii::$app->db->createCommand("SELECT min(rangos.exp_necesaria)-$exp As faltante, nivel,profesiones.nombres As prof FROM rangos"
                    . " join fabrican on fabrican.id_utilizable='.$id.'"
                    . " join profesiones ON fabrican.id_profesion = profesiones.id"
                    . " where exp_necesaria > $exp")->queryAll();
       }
       
       return $this->renderPartial('_modal',[
                'dataProvider'=>$dataProvider,
                'dataProvider2'=>$dataProvider2,
                'objeto' => $nombre,
                'exp'=>$exp,
                'id'=>$id,
                'nombre'=>$nombre,
                'rango'=>$rango,
                'cantidad' => $cantidad
              
                
       ]);
       

    }


}
