<?php

namespace app\controllers;

use app\models\Zonas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\imagine\Image;
/**
 * ZonasController implements the CRUD actions for Zonas model.
 */
class ZonasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Zonas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Zonas::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zonas model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zonas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Zonas();

       if ($model->load(Yii::$app->request->post())) {
           
            $image = $model->uploadImage();
            if($image != null){
                if ($model->save()) {
                    

                        $path = $model->getImageFile();
                        $image->saveAs($path);
                        
                    return $this->redirect(['view', 'id'=>$model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'No se pudo crear el registro, recuerde insertar el registro.');
            }
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zonas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model = $this->findModel($id);
        $oldFile = $model->getImageFile();
        $oldIcono = $model->mapa;
        
        if ($model->load(Yii::$app->request->post())) {
         
            $image = $model->uploadImage();

            if ($image === false) {
                $model->mapa = $oldIcono;
            }
            
                if ($model->save()) {
                    if ($image !== false && unlink($oldFile)) { 
 
                        $path = $model->getImageFile();                   
                        $image->saveAs($path);
                 }
                return $this->redirect(['view', 'id'=>$model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'No se pudo crear el registro o no se ha modificado la imagen, recuerde insertar el registro.');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zonas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
       $model = $this->findModel($id);
        if ($model->delete()) {
            if (!$model->deleteImage()) {
                Yii::$app->session->setFlash('error', 'No se encontro la imagen para eliminar');
            }
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zonas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Zonas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zonas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
