<?php

namespace app\controllers;

use app\models\Componentes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\imagine\Image;
Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/img/iconos/';
Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/web/img/iconos/';

/**
 * ComponentesController implements the CRUD actions for Componentes model.
 */
class ComponentesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Componentes models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Componentes::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Componentes model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Componentes();
        if ($model->load(Yii::$app->request->post())) {
            // process uploaded image file instance
            $image = $model->uploadImage();
            if($image != null){
                if ($model->save()) {
                    // upload only if valid uploaded file instance found

                        $path = $model->getImageFile();
                        $image->saveAs($path);
                        Image::thumbnail(Yii::$app->basePath . '/web/img/iconos/'.$model->icono, 160, 90)->save(Yii::$app->basePath . '/web/img/thumbs/'.'thumb_' . $model->icono, ['quality' => 90]);

                    return $this->redirect(['view', 'id'=>$model->id]);
                }
            } else {
                Yii::$app->session->setFlash('error', 'No se pudo crear el registro, recuerde insertar el registro.');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFile = $model->getImageFile();
        $oldIcono = $model->icono;
        $oldthumbs = $model->getThumbsFile();
        

        if ($model->load(Yii::$app->request->post())) {
            // process uploaded image file instance
            $image = $model->uploadImage();

            // revert back if no valid file instance uploaded
            if ($image === false) {
                $model->icono = $oldIcono;
            }
            
                if ($model->save()) {
                    if ($image !== false && unlink($oldFile) && unlink($oldthumbs)) { // delete old and overwrite
                // upload only if valid uploaded file instance found
                
                    $path = $model->getImageFile();
                    
                    $image->saveAs($path);
                    Image::thumbnail(Yii::$app->basePath . '/web/img/iconos/'.$model->icono, 160, 90)->save(Yii::$app->basePath . '/web/img/thumbs/'.'thumb_' . $model->icono, ['quality' => 90]);
                }
                return $this->redirect(['view', 'id'=>$model->id]);
            } else {
                Yii::$app->session->setFlash('error', 'No se pudo crear el registro o no se ha modificado la imagen, recuerde insertar el registro.');
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Componentes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            if (!$model->deleteImage()) {
                Yii::$app->session->setFlash('error', 'No se encontro la imagen para eliminar');
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Componentes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Componentes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Componentes::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
