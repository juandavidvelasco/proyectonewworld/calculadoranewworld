<?php

namespace app\controllers;

use app\models\Rangos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * RangosController implements the CRUD actions for Rangos model.
 */
class RangosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Rangos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Rangos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_profesion' => SORT_DESC,
                    'nivel' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rangos model.
     * @param int $id_profesion Id Profesion
     * @param int $nivel Nivel
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_profesion, $nivel)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_profesion, $nivel),
        ]);
    }

    /**
     * Creates a new Rangos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Rangos();
        $profesiones = Yii::$app->db->createCommand("select id,nombres from profesiones")->queryAll();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_profesion' => $model->id_profesion, 'nivel' => $model->nivel]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'profesiones' => $profesiones
        ]);
    }

    /**
     * Updates an existing Rangos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_profesion Id Profesion
     * @param int $nivel Nivel
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_profesion, $nivel)
    {
        $profesiones = Yii::$app->db->createCommand("select id,nombres from profesiones")->queryAll();
        $model = $this->findModel($id_profesion, $nivel);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_profesion' => $model->id_profesion, 'nivel' => $model->nivel]);
        }

        return $this->render('update', [
            'model' => $model,
            'profesiones' => $profesiones
        ]);
    }

    /**
     * Deletes an existing Rangos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_profesion Id Profesion
     * @param int $nivel Nivel
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_profesion, $nivel)
    {
        $this->findModel($id_profesion, $nivel)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rangos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_profesion Id Profesion
     * @param int $nivel Nivel
     * @return Rangos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_profesion, $nivel)
    {
        if (($model = Rangos::findOne(['id_profesion' => $id_profesion, 'nivel' => $nivel])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
