<?php

namespace app\controllers;

use app\models\FabricanIngrediente;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Componentes;

use Yii;

/**
 * FabricanIngredientesController implements the CRUD actions for FabricanIngrediente model.
 */
class FabricanIngredientesController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all FabricanIngrediente models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => FabricanIngrediente::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FabricanIngrediente model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FabricanIngrediente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $utilizable = Yii::$app->db->createCommand("select id,nombre from utilizables")->queryAll();
        $profesiones = Yii::$app->db->createCommand("select id,nombres from profesiones")->queryAll();
        $ingredientes = Yii::$app->db->createCommand("select id,nombre from ingredientes")->queryAll();
        
        $model = new FabricanIngrediente();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'utilizable'=>$utilizable,
            'profesiones'=>$profesiones,
            'ingredientes'=>$ingredientes
        ]);
    }

    /**
     * Updates an existing FabricanIngrediente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $utilizable = Yii::$app->db->createCommand("select id,nombre from utilizables")->queryAll();
        $profesiones = Yii::$app->db->createCommand("select id,nombres from profesiones")->queryAll();
        $ingredientes = Yii::$app->db->createCommand("select id,nombre from ingredientes")->queryAll();
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'utilizable'=>$utilizable,
            'profesiones'=>$profesiones,
            'ingredientes'=>$ingredientes
        ]);
    }

    /**
     * Deletes an existing FabricanIngrediente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FabricanIngrediente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return FabricanIngrediente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FabricanIngrediente::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
